### Preamble {{{
##  ==========================================================================
##        @file Makefile
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-05 Friday 03:58:33 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 18:09:39 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
override __R_FILE__ := $(lastword $(MAKEFILE_LIST))
override __R_DIR__  := $(dir      $(__R_FILE__))
##
ifneq "$(__R_FILE__)" "$(firstword $(MAKEFILE_LIST))"
$(error $(__R_FILE__): Include not allowed)
endif
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
.NOTPARALLEL:
##
.SECONDEXPANSION:
##
ifdef SILENT
# ----------------------------------------------------------------------------
# NOTE:
#
.SILENT:
#
# Should deliver the same behavior as:
#
#   MAKEFLAGS += --silent
# ----------------------------------------------------------------------------
endif
##
.SUFFIXES:
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override SUFFIXES :=
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define \s :=
$() $()
endef
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
ifndef &
override define &
$(firstword $|)
endef
else
$(error Defined special variable '&': reserved for internal use)
endif
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define s/\s/\\s/
$(subst $(\s),\$(\s),$1)
endef
##
override define s/\\s/\s/
$(subst \$(\s),$(\s),$1)
endef
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override __FILE__ := $(abspath $(__R_FILE__))
override __DIR__  := $(abspath $(__R_DIR__))
##
ifdef VERBOSE
$(info make: Reading makefile '$(__FILE__)')
endif
##
override __FILE__ := $(call s/\s/\\s/,$(__FILE__))
override __DIR__  := $(call s/\s/\\s/,$(__DIR__))
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
ifndef PREFIX
ifeq "$(PWD)" "$(CURDIR)"
override PREFIX  ?= ../../
else
override PREFIX  := $(CURDIR)/
endif
else
override PREFIX  := $(call s/\\s/\s/,$(PREFIX))
endif
ifndef GIT_DIR
override GIT_DIR := $(PWD)/.git
else
override GIT_DIR := $(call s/\\s/\s/,$(GIT_DIR))
endif
##
override PREFIX  := $(call s/\s/\\s/,$(PREFIX))
override GIT_DIR := $(call s/\s/\\s/,$(GIT_DIR))
##
override GIT_HOOKS_DIR := $(GIT_DIR)/hooks
override GIT_HOOKS     := commit-msg prepare-commit-msg pre-commit pre-receive
override GIT_HOOKS     := $(addprefix $(GIT_HOOKS_DIR)/,$(GIT_HOOKS))
# ----------------------------------------------------------------------------
##
### Git Hooks {{{
##  ==========================================================================
### All {{{
##  ==========================================================================
# Default
.PHONY: all
all: install
##  ==========================================================================
##  }}} All
##
### Install {{{
##  ==========================================================================
.PHONY: install
install: hooks
##  ==========================================================================
##  }}} Install
##
### Hooks {{{
##  ==========================================================================
.PHONY: hooks
hooks: $(GIT_HOOKS)
##  ==========================================================================
##  }}} Hooks
##
$(GIT_HOOKS): | $$(notdir $$(lastword $$@)) $(GIT_HOOKS_DIR)
	@$(if $(VERBOSE),printf "make: Creating symbolic link '%s' -> '%s'\n"\
	   '$@' $(PREFIX)$&)
	@ln -s $(PREFIX)$& '$@'
##
$(GIT_HOOKS_DIR): | $(GIT_DIR)
##  ==========================================================================
##  }}} Git Hooks
##
# ----------------------------------------------------------------------------
$(MAKEFILE_LIST):
	@
##
%:: %,v
%:: RCS/%
%:: RCS/%,v
%:: SCCS/s.%
%:: s.%
