Git Hooks
=========

Looking for more advanced project management automation?
========================================================

If you are looking for more advanced project management automation, then my
[Make Scripts][] are at your service:

> A set of robust (even paths containing spaces are fully supported) and
> configurable [Make][Make/Wikipedia] scripts which constitute a convenient
> front end to tedious project management manipulations over
> [CMake][CMake/Wikipedia], [Git][Git/Wikipedia], [Python][Python/Wikipedia]
> [Virtual Environment][], and more.

Table of Contents
=================

-   [About](#markdown-header-about)
-   [Installation](#markdown-header-installation)
    -   [Requirements](#markdown-header-requirements)
        -   [Platforms](#markdown-header-platforms)
    -   [Instructions](#markdown-header-instructions)

About
=====

State of the art [Git][Git/Wikipedia] hooks implemented as highly portable
[POSIX][POSIX/Wikipedia]-compliant shell scripts.

**NOTE:**
> Find out more information about this initiative in my [Stack Overflow
> question/answer][Git Hooks/Stack Overflow].

Installation
============

Requirements
------------

### Platforms

-   GNU/Linux;
-   MSYS2;
-   Cygwin.

Instructions
------------

1.  Make sure that a target project is being developed with a compatible
    platform (see [Requirements](#markdown-header-requirements));

2.  In an arbitrary Git repository (of the target project), run

        :::bash
        git submodule add https://bitbucket.org/Alexander-Shukaev/git-hooks.git <git-hooks-dir>

    where, for example, the default recommended (but not enforced) value for
    `<git-hooks-dir>` is `share/git/hooks`;

3.  In order to install the Git hooks management of the target project, run
    (preferably in the root directory)

        :::bash
        make -C <git-hooks-dir>

    using [Make][Make/Wikipedia];

4.  Enable desired features and fine-tune their behavior by adding the
    optional `.gitconfig` configuration file (to the root directory), e.g.:

        :::ini
        [clangformat]
          enabled = true

        [columnlimit]
          enabled    = true
          extensions = -in,-md,-rst,-txt
          size       = 78

        [commit]
          enabled = true

        [whitespace]
          enabled = true

    **NOTE:**
    > The behavior of the `whitespace` feature is configured with the standard
    > `.gitattributes` configuration file.

5.  Enjoy, and happy Gitting!  `;)`

    **NOTE:**
    > [Make Scripts][] also fully support automation on top of this project
    > ([Git Hooks][]).

[Make/Wikipedia]: http://en.wikipedia.org/wiki/Make_(software)

[CMake/Wikipedia]: http://en.wikipedia.org/wiki/CMake

[Git/Wikipedia]: http://en.wikipedia.org/wiki/Git

[Python/Wikipedia]: http://en.wikipedia.org/wiki/Python_(programming_language)

[Virtual Environment]: http://docs.python.org/3/tutorial/venv.html

[POSIX/Wikipedia]: http://en.wikipedia.org/wiki/POSIX

[Git Hooks/Stack Overflow]: http://stackoverflow.com/q/33924519

[Git Hooks]:    ../..
[Make Scripts]: ../../../make-scripts
