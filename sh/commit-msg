#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file commit-msg
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-01-14 Monday 02:50:54 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-11-23 Monday 01:35:29 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
. "$(dirname "$(readlink -f "${0}")")/.global"
##
death() {
  :
}
##
main() {
  config_is_work_tree_first=0
  hook="${0##*/}"
  if hook --enabled; then
    :
  else
    return 0
  fi
  file="${1}"
  msg="$(cat "${file}")"
  cc="$(stdout '%s' "${msg}" | tail -n 1 | sed 's/^\(.\).*/\1/')"
  cc="${cc:-#}"
  msg="$(stdout '%s' "${msg}" | grep -v -e "^${cc}")"
  if whitespace --enabled; then
    if diff="$(stdout '%s' "${msg}" | grep -n -e '[[:blank:]]$')"; then
      die '%s\n%s' 'Whitespace issues detected' "${diff}"
    else
      stdout "%s${diff:+\n}" "${diff}"
    fi
  fi
  if column_limit --enabled; then
    text="$(stdout '%s' "${msg}" | head -n 1)"
    if diff="$(stdout '%s' "${text}" | grep -n -e '^..\{72\}')"; then
      die '%s\n%s' "Column limit (72) violation detected" "${diff}"
    else
      stdout "%s${diff:+\n}" "${diff}"
    fi
    text="$(stdout '%s' "${msg}" | tail -n +2)"
    if diff="$(stdout '\n%s' "${text}" | grep -n -e '^..\{72\}')"; then
      die '%s\n%s' "Column limit (72) violation detected" "${diff}"
    else
      stdout "%s${diff:+\n}" "${diff}"
    fi
  fi
  text="$(stdout '%s' "${msg}" | sed -n 2p)"
  if diff="$(stdout '\n%s' "${text}" | grep -n -e '^..*$')"; then
    die '%s\n%s' "Empty line violation detected" "${diff}"
  else
    stdout "%s${diff:+\n}" "${diff}"
  fi
  death
  return 0
}
##
main "${@}"
